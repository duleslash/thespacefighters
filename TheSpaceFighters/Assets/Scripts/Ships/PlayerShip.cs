﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerShip : Ship
{
		public string playerName = "Noname";
		public int score = 0;
		public int lives = 3;
		public static PlayerShip instance;
		public float movingSpeed = 4;
		public GameObject onIndestructibleEffectsHolder;
		public ShowHideButton gameoverShow;
		public List<GameObject> weaponUpgradeHolders;
		private List<Weapon>  startWeapons;
		private int weaponUpgraded = 0;
		private List<Effect> onIndestructibleEffects;
		private float indestructibleOnRespawnTime = 5;
		private Vector4 viewPortBounds;
		private Vector3 startPosition;
		private Controls controls;

		public override void OnEnable ()
		{
				graphicsZ = 11;// set up graphics
				base.OnEnable ();

				

				startPosition = transform.position;
				viewPortBounds = LevelCreator.GetScreenBounds (0.9f);
				instance = this;

				indestructibleOnRespawnTime = 4;

				if (onIndestructibleEffects == null) {
						startWeapons = new List<Weapon> ();
						startWeapons.AddRange (weapons);
						onIndestructibleEffects = new List<Effect> ();//indestructible effect
						onIndestructibleEffects.AddRange (onIndestructibleEffectsHolder.GetComponents<Effect> ());//if its start of the game
				} else {
						//respawn
						// we lost 3 weapon levels
						if (weaponUpgraded > 3) {
								weaponUpgraded -= 3;
								UpgradeWeapon ();
						} else {
								weaponUpgraded = 0;
								weapons = new List<Weapon> ();
								weapons.AddRange (startWeapons);
						}
						// if its respawn
						Effect.PlayEffects (onIndestructibleEffects);
						shipCollider.enabled = false;
				}
				controls = GetComponent<Controls> ();

		}

		public override System.Void Update ()
		{
				base.Update ();
		}

		public override System.Void AliveUpdate (float deltaTime)
		{
				float horizontal = controls.GetAxis ().x;
				float vertical = controls.GetAxis ().y;
				
				Indestructible ();

				//controls
				transform.position += new Vector3 (horizontal, vertical, 0) * movingSpeed * deltaTime;
				if (controls.GetFireKey ())
						Shoot ();

				//dont let it go out of bounds
				if (viewPortBounds.x > transform.position.x)
						transform.position = new Vector3 (viewPortBounds.x, transform.position.y, transform.position.z);
				if (viewPortBounds.z < transform.position.x)
						transform.position = new Vector3 (viewPortBounds.z, transform.position.y, transform.position.z);
				if (viewPortBounds.y > transform.position.y)
						transform.position = new Vector3 (transform.position.x, viewPortBounds.y, transform.position.z);
				if (viewPortBounds.w < transform.position.y)
						transform.position = new Vector3 (transform.position.x, viewPortBounds.w, transform.position.z);
	
				base.AliveUpdate (deltaTime);
		}

		void Indestructible ()
		{
				indestructibleOnRespawnTime -= Time.deltaTime;
				if (!shipCollider.enabled && indestructibleOnRespawnTime <= 0) {
						Effect.StopEffects (onIndestructibleEffects);
						shipCollider.enabled = true;
				}
		}

		public override void Shoot ()
		{
				base.Shoot ();
		}

		public override void Hit (float damage)
		{
				base.Hit (damage);
		}

		public override void Death ()
		{
				lives--;
				base.Death ();
		}

		public override void Reset ()
		{
				base.Reset ();
				transform.position = startPosition;
				if (lives > 0)
						Invoke ("Respawn", 1);//we want to respawn it we have lives
				else 
						gameoverShow.OnMouseDown (); // we dont have lives left so its game over
		}

		public override void OnTriggerEnter (Collider other)
		{

				base.OnTriggerEnter (other);
		}

		public void UpgradeWeapon ()
		{
				if (weaponUpgraded < weaponUpgradeHolders.Count) {
						GameObject holder = weaponUpgradeHolders [weaponUpgraded];
						holder.transform.parent = transform;
						holder.transform.localPosition = Vector3.zero;
						weaponsHolder = holder;
						weapons = new List<Weapon> ();
						if (weaponsHolder != null)
								weapons.AddRange (weaponsHolder.GetComponents<Weapon> ());

						weaponUpgraded++;
				} else
						PlayerShip.instance.score += Random.Range (40, 70); // if we dont have more weapon upgrades we better get score :)
		}

		public void Respawn ()
		{
				gameObject.SetActive (true);
		}
}
