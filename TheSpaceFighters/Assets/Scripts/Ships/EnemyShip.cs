﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyShip : Ship
{

		public static List<int> freeZAxis = new List<int> (new int[] {
				12,
				13,
				14,
				15,
				16,
				17,
				18,
				19,
				20,
				21,
				22,
				23,
				24,
				25,
				26,
				27,
				28,
				29,
				30
		});
		private  List<Effect> movementEffects;

		public override void OnEnable ()
		{
				//draw graphics on freeZAxis
				if (freeZAxis.Count > 0) {
						graphicsZ = freeZAxis [0];
						freeZAxis.RemoveAt (0);
				}
				movementEffects = new List<Effect> ();
				if (transform.GetComponent<Effect> ())
						movementEffects.AddRange (transform.GetComponents<Effect> ());
				base.OnEnable ();
		}

		public override System.Void AliveUpdate (float deltaTime)
		{

				base.AliveUpdate (deltaTime);
				if (movementEffects.Count < 1)
						movementEffects.AddRange (transform.GetComponents<Effect> ());
				else if (Effect.AreFinnishedEffects (movementEffects)) {
						Reset (); //if flight is over
				}
				Shoot ();
		}
	
		public override void Death ()
		{
				PlayerShip.instance.score += Random.Range (1, 4) * Mathf.FloorToInt (maxHp);//it has been killed player gets score
				PullGameObjectManager.PullGameObject ("Drop", transform.position, Vector3.zero);//it also drops drop
				Effect.DestroyEffects (movementEffects);//remove effects so we could pull it again
				base.Death ();
		}
	
		public override void Reset ()
		{
				Effect.DestroyEffects (movementEffects);
				base.Reset ();
				freeZAxis.Add (graphicsZ);//return graphics z for someone else to use
				transform.parent.GetComponent<PullGameObjectManager> ().ReturnGOToPull (gameObject);//return object to pull
	
		}
}
