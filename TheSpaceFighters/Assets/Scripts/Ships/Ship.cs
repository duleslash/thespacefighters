﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Ship : MonoBehaviour
{

		public enum ShipState
		{
				Spawning,
				Alive,
				Death
		}
		;
		protected ShipState state;
		// holder with graphics we will drow on free z Axis (to avoid ships overlaping)
		public GameObject graphicsHolder;
		protected int graphicsZ;

		// hit points
		[HideInInspector]
		public float
				maxHp;
		public float hp;
		public GameObject weaponsHolder;
	
		//effects
		public GameObject onSpawnEffectsHolder;
		public GameObject onHitEffectsHolder;
		public GameObject onDeathEffectsHolder;

		//effects
		protected List<Effect> onSpawnEffects;
		protected List<Effect> onHitEffects;
		protected List<Effect> onDeathEffects;

		//List of current weapons
		protected List<Weapon> weapons;
		protected Vector3 graphicsScale;
		public float turnAngle = 60;
		public float turnSpeed = 5;
		private Vector3 lastpos = Vector3.zero;
		[HideInInspector]
		public Collider
				shipCollider;
		public static List<Ship> allShipsInScene = new List<Ship> ();

		public virtual void OnEnable ()
		{

				allShipsInScene.Add (this);

				shipCollider = GetComponentInChildren<Collider> ();

				onSpawnEffects = new List<Effect> ();
				if (onSpawnEffectsHolder != null)
						onSpawnEffects.AddRange (onSpawnEffectsHolder.GetComponents<Effect> ());
				onHitEffects = new List<Effect> ();
				if (onHitEffectsHolder != null)
						onHitEffects.AddRange (onHitEffectsHolder.GetComponents<Effect> ());
				onDeathEffects = new List<Effect> ();
				if (onDeathEffectsHolder != null)
						onDeathEffects.AddRange (onDeathEffectsHolder.GetComponents<Effect> ());

				weapons = new List<Weapon> ();
				if (weaponsHolder != null)
						weapons.AddRange (weaponsHolder.GetComponents<Weapon> ());

				transform.position = new Vector3 (transform.position.x, transform.position.y, 0);
				Effect.PlayEffects (transform, onSpawnEffects);
				graphicsHolder.transform.localPosition -= Vector3.up * graphicsZ;
				state = ShipState.Spawning;
				maxHp = hp;
				graphicsScale = graphicsHolder.transform.localScale;
				lastpos = transform.position;
		}

		public virtual void Update ()
		{
				switch (state) {
				case ShipState.Spawning:
						if (Effect.AreFinnishedEffects (onSpawnEffects))
								state = ShipState.Alive;
						break;
				case ShipState.Alive:
						AliveUpdate (Time.deltaTime);
						break;
				case ShipState.Death:
						if (Effect.AreFinnishedEffects (onDeathEffects))
								Reset ();
			
						break;
				}
		}

		public virtual void AliveUpdate (float deltaTime)
		{


				graphicsHolder.transform.localRotation = Quaternion.Lerp (graphicsHolder.transform.localRotation, Quaternion.AngleAxis (new Vector3 (transform.InverseTransformPoint (lastpos).x, 0, 0).normalized.x * turnAngle, Vector3.forward), Time.deltaTime * turnSpeed);
				lastpos = transform.position;
		}

		public virtual void Shoot ()
		{
				foreach (Weapon w in weapons)
						w.Shoot (transform.tag);
		}

		public virtual void Hit (float damage)
		{
				PullGameObjectManager.PullGameObject ("Explosion2", transform.position, transform.eulerAngles);
				hp -= damage;
				Effect.PlayEffects (transform, onHitEffects);
				if (hp <= 0 && state != ShipState.Death)
						Death ();
		}

		public virtual void Death ()
		{
				graphicsHolder.transform.localPosition = Vector3.zero - Vector3.up * 46;
				PullGameObjectManager.PullGameObject ("Explosion", transform.position, transform.eulerAngles);
				state = ShipState.Death;
				hp = 0;
				Effect.PlayEffects (graphicsHolder.transform, onDeathEffects);
				shipCollider.enabled = false;

		}

		public virtual void Reset ()
		{
				graphicsHolder.transform.localPosition = Vector3.zero;
				graphicsHolder.transform.localScale = graphicsScale;
				graphicsHolder.transform.localEulerAngles = Vector3.zero;
				shipCollider.enabled = true;
				gameObject.SetActive (false);
				hp = maxHp;
				allShipsInScene.Remove (this);
		}
	
		public virtual void OnTriggerEnter (Collider other)
		{

				if (other.transform.tag != transform.tag) {
						if (other.transform.GetComponent<Ship> () && transform.tag == "Player") {
								Hit (other.transform.GetComponent<Ship> ().hp);
								other.transform.GetComponent<Ship> ().Hit (hp);
				
						}
						if (other.transform.tag == "Boss")
								Hit (1000);
						if (other.transform.GetComponent<Missile> ()) {
								Hit (other.transform.GetComponent<Missile> ().damage);
								other.transform.GetComponent<Missile> ().Reset ();
						}
				}
		}
}
