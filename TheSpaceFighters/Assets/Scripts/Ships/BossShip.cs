﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BossShip : Ship
{

		public List<GameObject> randomEffects;
		private  List<Effect> effects;
		private int rndEffect;

		public override void OnEnable ()
		{
				//we should stop time till boss is killed
				LevelCreator.instance.enabled = false;
				graphicsZ = 10;
				base.OnEnable ();
		}
		//chose random effect for boss
		void TakeRandomMovement ()
		{
				// we take random group of effect for our boss
				if (randomEffects.Count <= 0)
						return;
				randomEffects [rndEffect].SetActive (false);
				effects = new List<Effect> ();
				rndEffect = Random.Range (0, randomEffects.Count);
				if (randomEffects [rndEffect].GetComponent<Effect> ())
						effects.AddRange (randomEffects [rndEffect].GetComponentsInChildren<Effect> (true));
				
				randomEffects [rndEffect].SetActive (true);
		}
	
		public override System.Void AliveUpdate (float deltaTime)
		{
				//if he finished last behaviour chose random effects again
				if (effects == null || Effect.AreFinnishedEffects (effects))
						TakeRandomMovement ();
				base.AliveUpdate (deltaTime);
		}

		public override void Reset ()
		{
				//turn on time to create next enemies
				LevelCreator.instance.enabled = true;
				base.Reset ();
		}

		public override void Death ()
		{
				Effect.StopEffects (effects);
				randomEffects [rndEffect].SetActive (false);
				PullGameObjectManager.PullGameObject ("Drop", transform.position, Vector3.zero);
				PlayerShip.instance.score += Random.Range (1, 4) * Mathf.FloorToInt (maxHp);
				base.Death ();
		}
}
