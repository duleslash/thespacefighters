using UnityEngine;
using System.Collections;

public class EnterScore : MonoBehaviour
{

		void OnMouseDown ()
		{
				if (!string.IsNullOrEmpty (PlayerShip.instance.playerName)) {
						int index = 0;
						while (!string.IsNullOrEmpty(PlayerPrefs.GetString("name_"+index.ToString())))
								index++;

						PlayerPrefs.SetString ("name_" + index.ToString (), PlayerShip.instance.playerName);
						PlayerPrefs.SetInt ("score_" + index.ToString (), PlayerShip.instance.score);
				}
		}
}
