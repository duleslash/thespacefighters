﻿using UnityEngine;
using System.Collections;

public class PrintHP : MonoBehaviour
{

		public ScaleToEffect onChangeEffect;
		private float curScale;

		void Start ()
		{
				curScale = PlayerShip.instance.hp / PlayerShip.instance.maxHp;
		}
	
		void Update ()
		{
				if (curScale != PlayerShip.instance.hp / PlayerShip.instance.maxHp) {
						if (onChangeEffect != null && !onChangeEffect.IsPlaying ()) {
								curScale = PlayerShip.instance.hp / PlayerShip.instance.maxHp;
								onChangeEffect.scaleTo = new Vector3 (curScale, 1, 1);
								onChangeEffect.PlayEffect ();

						}
				}
		}
}
