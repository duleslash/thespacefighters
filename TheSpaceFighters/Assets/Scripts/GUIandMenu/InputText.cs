using UnityEngine;
using System.Collections;

public class InputText : MonoBehaviour {
	public Font font;
	
	void OnGUI() {
		GUIStyle TextStyle = new GUIStyle();
		TextStyle.normal.textColor = Color.white;
		TextStyle.font = font;
		TextStyle.fontSize = Screen.width/30;
		PlayerShip.instance.playerName = GUI.TextField(new Rect(0.58f*Screen.width,0.55f*Screen.height,300,40), PlayerShip.instance.playerName, 25,TextStyle);
	}
}
