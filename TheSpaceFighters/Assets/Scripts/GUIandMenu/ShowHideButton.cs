﻿using UnityEngine;
using System.Collections;

public class ShowHideButton : MonoBehaviour
{

		public Effect onOverEffect;
		public Effect onOutEffect;
		public GameObject[] toShow;
		public GameObject[] toHide;

		void OnMouseEnter ()
		{
				if (onOverEffect != null)
						onOverEffect.PlayEffect ();
		}

		void OnMouseExit ()
		{
				if (onOutEffect != null)
						onOutEffect.PlayEffect ();
		}

		public void OnMouseDown ()
		{
				OnMouseExit ();
				foreach (GameObject g in toShow)
						g.SetActive (true);
				foreach (GameObject g in toHide)
						g.SetActive (false);
		}

}
