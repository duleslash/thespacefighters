﻿using UnityEngine;
using System.Collections;

public class PauseButton : MonoBehaviour
{
		public float timeScale = 0;

		void Update ()
		{
				if ((Input.GetKeyDown (KeyCode.P) || Input.GetKeyDown (KeyCode.Escape)) && Time.timeScale == 1)
						transform.SendMessage ("OnMouseDown");
		}

		void OnMouseDown ()
		{
				Time.timeScale = timeScale;
		}
}
