﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ReadScore : MonoBehaviour
{
		public int scoreindex;
		public TextMesh nameTM;
		public TextMesh scoreTM;
		public static List<ReadedScore> scores;

		public struct ReadedScore
		{
				public int score;
				public string name;

				public ReadedScore (int setscore, string setname)
				{
						score = setscore;
						name = setname;
				}
		}

		// Use this for initialization
		void OnEnable ()
		{
				if (scoreindex == 0)
						ReadScores ();
				
		}

		void Update ()
		{
				if (scores.Count > scoreindex) {
						if (nameTM.text != scores [scoreindex].name)
								nameTM.text = scores [scoreindex].name;
						if (scoreTM.text != scores [scoreindex].score.ToString ())
								scoreTM.text = scores [scoreindex].score.ToString ();
						nameTM.color = Color.white;
						scoreTM.color = Color.white;
				} else {
						nameTM.color = Color.gray;
						scoreTM.color = Color.gray;
				}
		this.enabled = false;
		
		}
	
		void ReadScores ()
		{
				scores = new List<ReadedScore> ();
				int index = 0;
				while (!string.IsNullOrEmpty(PlayerPrefs.GetString("name_"+index.ToString()))) {
						scores.Add (new ReadedScore (PlayerPrefs.GetInt ("score_" + index.ToString ()), PlayerPrefs.GetString ("name_" + index.ToString ())));
						index++;
				}
				scores.Sort (delegate(ReadedScore rs1, ReadedScore rs2) {
						return rs2.score.CompareTo (rs1.score);
				});


		}

}
