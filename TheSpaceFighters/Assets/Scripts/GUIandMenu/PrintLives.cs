﻿using UnityEngine;
using System.Collections;

public class PrintLives : MonoBehaviour
{
		public Effect onChangeEffect;

		void Start ()
		{
				GetComponent<TextMesh> ().text = PlayerShip.instance.score.ToString ();
		}

		void Update ()
		{
				if (GetComponent<TextMesh> ().text != PlayerShip.instance.lives.ToString ()) {
						if (onChangeEffect != null && !onChangeEffect.IsPlaying ())
								onChangeEffect.PlayEffect ();
						GetComponent<TextMesh> ().text = PlayerShip.instance.lives.ToString ();
				}
		}
}
