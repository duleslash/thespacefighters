﻿using UnityEngine;
using System.Collections;

public class SwitchButton : MonoBehaviour
{
		public string value;
		public Effect onOverEffect;
		public Effect onOutEffect;
	
		void OnMouseEnter ()
		{
				if (onOverEffect != null)
						onOverEffect.PlayEffect ();
		}
	
		void OnMouseExit ()
		{
				if (onOutEffect != null)
						onOutEffect.PlayEffect ();
		}
	
		void OnMouseDown ()
		{
				if (value.ToUpper () == "QUIT")
						Application.Quit ();
				else
						Application.LoadLevel (value);
		}
}
