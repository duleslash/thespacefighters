﻿using UnityEngine;
using System.Collections;

public class DockToScreen : MonoBehaviour
{

		public Vector2 viewportPosition;

		void OnEnable ()
		{
				transform.position = LevelCreator.GetPosition (viewportPosition);
		}
}
