﻿using UnityEngine;
using System.Collections;

public class MoveByEffect : Effect
{

		public Vector2 moveByPosition;
		public bool lookAtMovement = false;
		private Vector3 startPosition;
		private Vector3 moveToPosition;
		private Vector3 startEulerRotation;
	
		public override System.Void PlayEffect (Transform effectTarget)
		{
				base.PlayEffect (effectTarget);
				startPosition = target.position;
				startEulerRotation = target.eulerAngles;
				moveToPosition = startPosition + new Vector3 (moveByPosition.x, moveByPosition.y, 0);

				if (lookAtMovement)
						target.Rotate (Vector3.up, -Vector3.Angle (target.forward, timeValue * (moveToPosition - startPosition).normalized));

		}

		public override System.Void EffectUpdate (System.Single deltaTime)
		{
				target.position = Vector3.Lerp (startPosition, moveToPosition, Evaluate ());

				if (lookAtMovement) {
						if ((moveToPosition - startPosition).normalized.x > 0)
								target.Rotate (Vector3.up, -Vector3.Angle (target.forward, timeValue * (moveToPosition - startPosition).normalized) * deltaTime * 5);
						else
								target.Rotate (Vector3.up, Vector3.Angle (target.forward, timeValue * (moveToPosition - startPosition).normalized) * deltaTime * 5);
				}

				base.EffectUpdate (deltaTime);

		}

		public override System.Void ReturnToFinish ()
		{
				target.position = startPosition;
				target.eulerAngles = startEulerRotation;
				base.ReturnToFinish ();
		}
}
