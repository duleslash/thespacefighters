﻿using UnityEngine;
using System.Collections;

public class WaveMoveToEffect : Effect
{

		public Vector2 moveToPosition;
		public bool lookAtMovement = false;
		public bool lookAtPoint = false;
		private Vector3 startPosition;
		public float amplitude;
		public float frequency;
		private Vector3 moveToPosition3;
		private Vector3 startEulerRotation;

		public override System.Void PlayEffect (Transform effectTarget)
		{
				base.PlayEffect (effectTarget);
				moveToPosition3 = new Vector3 (moveToPosition.x, moveToPosition.y, target.position.z);
				startPosition = target.position;
				startEulerRotation = target.eulerAngles;
				/*if (lookAtMovement || lookAtPoint)
			target.Rotate (Vector3.up, -Vector3.Angle (target.forward, timeValue*(moveToPosition3 - startPosition).normalized));
		*/
		}

		public override System.Void EffectUpdate (System.Single deltaTime)
		{
				Vector3 rightVector = Vector3.Cross (target.up, (moveToPosition3 - startPosition).normalized);
				float waveTime = (curTime - delay) / duration;
				float wavePoint = Mathf.Sin (2 * Mathf.PI * (frequency * waveTime)) * amplitude;

				target.position = wavePoint * rightVector + Vector3.Lerp (startPosition, moveToPosition3, Evaluate ());

				float nextCurTime = curTime + timeValue * deltaTime;
				float nextWaveTime = (nextCurTime - delay) / duration;
				float nextWavePoint = Mathf.Sin (2 * Mathf.PI * (frequency * nextWaveTime)) * amplitude;
				Vector3 nextPoint = nextWavePoint * rightVector + Vector3.Lerp (startPosition, moveToPosition3, Evaluate (nextCurTime));
		
				if (lookAtMovement) {
						if (wavePoint > 0)
								target.Rotate (Vector3.up, timeValue * -Vector3.Angle (target.forward, (nextPoint - target.position).normalized));
						else
								target.Rotate (Vector3.up, timeValue * Vector3.Angle (target.forward, (nextPoint - target.position).normalized));
				}

				if (lookAtPoint) {
						if ((moveToPosition3 - startPosition).normalized.x > 0)
								target.Rotate (Vector3.up, -Vector3.Angle (target.forward, timeValue * (moveToPosition3 - startPosition).normalized) * deltaTime * 15);
						else
								target.Rotate (Vector3.up, Vector3.Angle (target.forward, timeValue * (moveToPosition3 - startPosition).normalized) * deltaTime * 15);
				}



				base.EffectUpdate (deltaTime);

		}

		public override System.Void StopEffect ()
		{
				base.StopEffect ();
				if (returnOnFinish) {
						target.position = startPosition;
						target.eulerAngles = startEulerRotation;
				}
		}

		public static WaveMoveToEffect AddComponetWithParametars (GameObject effectTarget, Vector2 moveTo, float effectDuration, float effectDelay, float effectAmplitude, float effectFrequency, Effect onFinish, bool lookAt)
		{
				WaveMoveToEffect added = effectTarget.AddComponent<WaveMoveToEffect> ();
				added.moveToPosition = moveTo;
				added.delay = effectDelay;
				added.duration = effectDuration;
				added.effectOnFinish = onFinish;
				added.amplitude = effectAmplitude;
				added.frequency = effectFrequency;
				added.lookAtMovement = lookAt;
				return added;
		}
}
