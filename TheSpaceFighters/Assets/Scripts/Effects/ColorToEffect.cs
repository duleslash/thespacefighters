﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ColorToEffect : Effect
{
	
		public Color colorTo = Color.white;
		private List<Renderer> renderers;
		private List<Color> startColors;

		public override System.Void PlayEffect (Transform effectTarget)
		{
				base.PlayEffect (effectTarget);

				renderers = new List<Renderer> ();
				foreach (Renderer r in target.GetComponentsInChildren<Renderer> ())
						if (r.material.HasProperty ("_Color"))
								renderers.Add (r);

				startColors = new List<Color> ();
				foreach (Renderer r in renderers)
						startColors.Add (r.material.color);
				
		}
	
		public override System.Void EffectUpdate (System.Single deltaTime)
		{
				for (int i=0; i < renderers.Count; i++)
						if (renderers [i].material.HasProperty ("_Color"))
								renderers [i].material.color = Color.Lerp (startColors [i], colorTo, Evaluate ());
				base.EffectUpdate (deltaTime);
		}
	
		public override System.Void StopEffect ()
		{
				base.StopEffect ();
				if (returnOnFinish) {
						for (int i=0; i < renderers.Count; i++) {
								if (renderers [i].material.HasProperty ("_Color"))
										renderers [i].material.color = startColors [i];
						}
				}
		}

		void OnDisable ()
		{
			if(renderers != null)
				for (int i=0; i < renderers.Count; i++) {
						if (renderers [i].material.HasProperty ("_Color"))
								renderers [i].material.color = startColors [i];
				}
		}
}
