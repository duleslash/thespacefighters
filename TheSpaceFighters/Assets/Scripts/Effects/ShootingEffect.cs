﻿using UnityEngine;
using System.Collections;

public class ShootingEffect : Effect
{

	
		public override System.Void EffectUpdate (System.Single deltaTime)
		{
				if (target.GetComponent<Weapon> ())
						target.GetComponent<Weapon> ().Shoot (target.tag);
				base.EffectUpdate (deltaTime);
		}
}
