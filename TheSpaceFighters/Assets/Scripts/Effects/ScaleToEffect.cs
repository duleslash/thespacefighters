﻿using UnityEngine;
using System.Collections;

public class ScaleToEffect : Effect
{
	
		public Vector3 scaleTo;
		private Vector3 startScale;
	
		public override System.Void PlayEffect (Transform effectTarget)
		{
				base.PlayEffect (effectTarget);
				startScale = target.localScale;
		}
	
		public override System.Void EffectUpdate (System.Single deltaTime)
		{
				target.localScale = Vector3.Lerp (startScale, scaleTo, Evaluate ());
				base.EffectUpdate (deltaTime);
		
		}

		public override System.Void ReturnToFinish ()
		{
				target.localScale = startScale;
				base.ReturnToFinish ();
		}
}
