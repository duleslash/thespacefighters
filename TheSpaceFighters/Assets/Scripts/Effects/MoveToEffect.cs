﻿using UnityEngine;
using System.Collections;

public class MoveToEffect : Effect
{

		public Vector2 moveToPosition;
		public bool lookAtMovement = false;
		public bool isViewport;
		private Vector3 startPosition;
		private Vector3 startEulerRotation;
		private Vector3 moveToPosition3;

		public override System.Void PlayEffect (Transform effectTarget)
		{
		
				base.PlayEffect (effectTarget);
				startPosition = target.position;
				startEulerRotation = target.eulerAngles;
				if (!isViewport)
						moveToPosition3 = new Vector3 (moveToPosition.x, moveToPosition.y, target.position.z);
				else {
						moveToPosition3 = LevelCreator.GetPosition (moveToPosition);
						moveToPosition3 += Vector3.forward * target.position.z;
				}
				/*	if (lookAtMovement)
		target.Rotate (Vector3.up, -Vector3.Angle (target.forward, timeValue*(moveToPosition3 - startPosition).normalized));
	*/
		}

		public override System.Void EffectUpdate (System.Single deltaTime)
		{
				target.position = Vector3.Lerp (startPosition, moveToPosition3, Evaluate ());

				if (lookAtMovement) {
						if (target.InverseTransformPoint (moveToPosition3).normalized.x > 0)
								target.Rotate (Vector3.up, Vector3.Angle (target.forward, timeValue * (moveToPosition3 - startPosition).normalized) * deltaTime * 5);
						else
								target.Rotate (Vector3.up, -Vector3.Angle (target.forward, timeValue * (moveToPosition3 - startPosition).normalized) * deltaTime * 5);
				}

				base.EffectUpdate (deltaTime);
		}

		public override System.Void ReturnToFinish ()
		{
				target.position = startPosition;
				target.eulerAngles = startEulerRotation;
				base.ReturnToFinish ();
		}

		public static MoveToEffect AddComponetWithParametars (GameObject effectTarget, Vector2 moveTo, float effectDuration, float effectDelay, Effect onFinish, bool lookAt)
		{
				MoveToEffect added = effectTarget.AddComponent<MoveToEffect> ();
				added.moveToPosition = moveTo;
				added.delay = effectDelay;
				added.duration = effectDuration;
				added.effectOnFinish = onFinish;
				added.lookAtMovement = lookAt;
				return added;
		}
}
