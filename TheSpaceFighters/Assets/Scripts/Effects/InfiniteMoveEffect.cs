﻿using UnityEngine;
using System.Collections;

public class InfiniteMoveEffect : Effect
{
	
		public float speed;
		public bool lookAtMovement = false;
		public float amplitude;
		public float frequency;
		private Vector3 startPosition;
		private float waveTime = 0;
		private float startAmplitude;
		private float startFrequency;

		public override System.Void PlayEffect (Transform effectTarget)
		{
				base.PlayEffect (effectTarget);
				startPosition = target.position;
				startAmplitude = amplitude;
				startFrequency = frequency;
				waveTime = 0;
		}
	
		public override System.Void EffectUpdate (System.Single deltaTime)
		{
				startPosition += target.forward * speed * Time.deltaTime;
				//calculating wave
				if (amplitude != 0 && frequency != 0) {
						if (waveTime * frequency > 1)// if finished a sinusoid then we should better reset that value - we dont want calculate with a large number when performing math functions
								waveTime = (waveTime * frequency - 1) / frequency;// reseting the waveTime value to the one we need to calculate
			
						waveTime += Time.deltaTime;
						target.position = startPosition + Mathf.Sin (2 * Mathf.PI * (frequency * waveTime)) * amplitude * target.right;
				} else
						target.transform.position = startPosition;
				//base.EffectUpdate (deltaTime);
		
		}
	
		public override System.Void StopEffect ()
		{
				base.StopEffect ();
				if (returnOnFinish) {
						amplitude = startAmplitude;
						frequency = startFrequency;

				}
		}
}
