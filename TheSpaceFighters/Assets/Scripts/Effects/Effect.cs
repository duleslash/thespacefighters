﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Effect : MonoBehaviour
{
		public Transform target;
		public bool autoStart = false;
		public float delay = 0;
		public float duration = 1;
		public bool isLooped = false;
		public AnimationCurve curve = new AnimationCurve (new Keyframe[] {
				new Keyframe (0, 0),
				new Keyframe (0.3f, 0.01f),
				new Keyframe (0.7f, 0.99f),
				new Keyframe (1, 1)
		});
		public Effect effectOnFinish;
		public bool returnOnFinish = false;
		protected float curTime = 0;
		protected float timeValue = 1;
		private bool isPlaying = false;
		public bool autoDestruct = false;
		// function to play effect on current transform if target is null
		public void PlayEffect ()
		{
				if (target == null)
						PlayEffect (transform);
				else
						PlayEffect (target);
		}
		// function to play effect on target
		public virtual void PlayEffect (Transform effectTarget)
		{
				if (isPlaying)//restart
						StopEffect ();
				if (target == null)
						target = effectTarget;
				curTime = 0;
				isPlaying = true;
				timeValue = 1;
		}
		// if autostart
		void OnEnable ()
		{
				if (autoStart)
						PlayEffect ();
		}
		// in normal update we call our Effect Update after delay
		void Update ()
		{
				if (isPlaying) {
						if (delay < curTime) {
								EffectUpdate (Time.deltaTime);
						} else
				if (timeValue < 0)
								timeValue = 1;
						curTime += Time.deltaTime * timeValue;
				}
		}
		//Here we change value that depands on curve evaluation
		//heck if its finished or loop it
		public virtual void EffectUpdate (float deltaTime)
		{
				if (duration < curTime - delay) {
						if (!isLooped)
								StopEffect ();
						else
								timeValue = -1;
				}
		}
		//Finish
		public virtual void StopEffect ()
		{
				isPlaying = false;
				if (effectOnFinish != null)
						effectOnFinish.PlayEffect ();
				if (returnOnFinish)
						ReturnToFinish ();
				if (autoDestruct)
						Destroy (this);
		}

		public virtual void ReturnToFinish ()
		{
		}
		//get evaluation - current state of our animation from 0-1
		public float Evaluate ()
		{
				return curve.Evaluate ((curTime - delay) / duration);
		}

		public float Evaluate (float curTime)
		{
				return curve.Evaluate ((curTime - delay) / duration);
		}

		public bool IsPlaying ()
		{
				return isPlaying;
		}

		public static void PlayEffects (List<Effect> effects)
		{
				foreach (Effect e in effects)
						e.PlayEffect ();
		}

		public static void PlayEffects (Transform target, List<Effect> effects)
		{
				foreach (Effect e in effects)
						e.PlayEffect (target);
		}

		public static bool AreFinnishedEffects (List<Effect> effects)
		{
				foreach (Effect e in effects)
						if (e.IsPlaying () && !e.isLooped)
								return false;
				StopEffects (effects);
				return true;
		}

		public static void StopEffects (List<Effect> effects)
		{
				foreach (Effect e in effects)
						e.StopEffect ();
		}

		public static void DestroyEffects (List<Effect> effects)
		{
				foreach (Effect e in effects)
						Destroy (e);
		}

		public static void ReturnAllEffects (List<Effect> effects)
		{
				foreach (Effect e in effects)
						e.ReturnToFinish ();
		}

		public static void ReturnUnmovableEffects (List<Effect> effects)
		{
				foreach (Effect e in effects)
						if (e.GetType () != typeof(MoveByEffect) && e.GetType () != typeof(MoveToEffect))
								e.ReturnToFinish ();
		}
}
