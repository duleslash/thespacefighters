﻿using UnityEngine;
using System.Collections;

public class TextureOffsetAnimator : MonoBehaviour
{

		public Vector2 speed = new Vector2 (0, 1);
	
		// Update is called once per frame
		void Update ()
		{
				//we move offset of materials main texture to make scrolling effect
				GetComponent<Renderer> ().material.mainTextureOffset += speed * Time.deltaTime;
		}
}
