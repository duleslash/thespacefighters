﻿using UnityEngine;
using System.Collections;

public class RandomRotateEffect : Effect
{

		public float speed = 500;
		private Vector3 startEulerRotation;
		private Vector3 rotationAxis;

		public override System.Void PlayEffect (Transform effectTarget)
		{
				base.PlayEffect (effectTarget);
				startEulerRotation = target.eulerAngles;
				rotationAxis = new Vector3 (Random.Range (-1f, 1f), Random.Range (-1f, 1f), Random.Range (-1f, 1f));
		}
	
		public override System.Void EffectUpdate (System.Single deltaTime)
		{
				target.Rotate (rotationAxis, Time.deltaTime * speed);
				base.EffectUpdate (deltaTime);
		
		}

		public override System.Void ReturnToFinish ()
		{
				target.eulerAngles = startEulerRotation;
				base.ReturnToFinish ();
		}
}
