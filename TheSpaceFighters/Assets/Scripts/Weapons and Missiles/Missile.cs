﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Missile : MonoBehaviour
{
		// holder with graphics we will drow z Axis
		public GameObject graphicsHolder;
		private int graphicsZ = 40;
		[HideInInspector]
		public float
				damage;

		//if tracking range is 0 it doesn't track target
		public float trackingRange = 0;
		public float angularSpeed = 10;
		private Ship trackingTarget;
		private InfiniteMoveEffect movement;
		private Vector4 viewPortBounds;

		void OnEnable ()
		{
				viewPortBounds = LevelCreator.GetScreenBounds (1.05f);
				transform.position = new Vector3 (transform.position.x, transform.position.y, 0); // fix Z
				graphicsHolder.transform.localPosition -= Vector3.up * graphicsZ;
				movement = GetComponent<InfiniteMoveEffect> ();
		}
		// Update is called once per frame
		void Update ()
		{

				if (trackingRange > 0) {
						//tracking range for target
						if (trackingTarget == null) {

								if (!movement.IsPlaying ())
										movement.PlayEffect ();
								foreach (Ship ship in Ship.allShipsInScene)
										if (Vector3.Distance (ship.transform.position, transform.position) < trackingRange && transform.tag != ship.transform.tag && ship.shipCollider.enabled)
												trackingTarget = ship;
						} else {
								if (Vector3.Distance (transform.position, trackingTarget.transform.position) < trackingRange) {
										if (transform.InverseTransformPoint (trackingTarget.transform.position).x < 0)
												transform.Rotate (Vector3.up, -Vector3.Angle (transform.forward, (trackingTarget.transform.position - transform.position).normalized) * Time.deltaTime * angularSpeed);
										else
												transform.Rotate (Vector3.up, Vector3.Angle (transform.forward, (trackingTarget.transform.position - transform.position).normalized) * Time.deltaTime * angularSpeed);
										if (!trackingTarget.gameObject.activeSelf || !trackingTarget.shipCollider.enabled)
												trackingTarget = null;
								} else
										trackingTarget = null;



						}
				}
				//raycast for hit
				RaycastHit hit;
				if (Physics.Raycast (transform.position, transform.forward, out hit, movement.speed * Time.deltaTime + 0.2f) && hit.transform.tag != transform.tag && hit.transform.GetComponent<Ship> ()) {
						hit.transform.GetComponent<Ship> ().Hit (damage);
						Reset ();
				}
				//if its not visible reset it and return to pull
				if (viewPortBounds.x > transform.position.x || viewPortBounds.z < transform.position.x || viewPortBounds.y > transform.position.y || viewPortBounds.w < transform.position.y)
						Reset ();
		}

		public void Reset ()
		{
				graphicsHolder.transform.localPosition = Vector3.zero;
				graphicsHolder.transform.localEulerAngles = Vector3.zero;
				transform.localPosition = Vector3.zero;
				movement.StopEffect ();
				transform.parent.GetComponent<PullGameObjectManager> ().ReturnGOToPull (gameObject);
				trackingTarget = null;
		}

		void OnDrawGizmos ()
		{
				if (trackingRange > 0)
						Gizmos.DrawWireSphere (transform.position, trackingRange);

		}
}
