﻿using UnityEngine;
using System.Collections;

public class CrossFireWeapon : Weapon
{

		public bool amplitudeCrossReverse = false;
		private int missileIndex = 0;
		private int changeAmplitude = -1;

		public override System.Void SendMissiles (string senderTag)
		{
				if (amplitudeCrossReverse)
						changeAmplitude *= -1;
				else
						changeAmplitude = 1;

				GameObject missile = PullGameObjectManager.PullGameObject (missileIDs [missileIndex], transform.position + missilePositions [missileIndex], transform.eulerAngles);
				if (missile != null && missile.GetComponent<Missile> ()) {
						Missile m = missile.GetComponent<Missile> ();
						m.damage = Random.Range (randomDamages [missileIndex].x, randomDamages [missileIndex].y);
						missile.transform.Rotate (Vector3.up, missileAngles [missileIndex]);
						m.GetComponent<InfiniteMoveEffect> ().amplitude *= changeAmplitude;
						m.transform.tag = senderTag;
				}
				missileIndex++;
				if (missileIndex >= missileIDs.Count)
						missileIndex = 0;
				//we dont want to send missiles with normal weapon
				//so we remove base.SendMissiles (senderTag);
		}
}
