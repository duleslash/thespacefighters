﻿using UnityEngine;
using System.Collections;

public class AimingPlayerWeapon : Weapon
{

		public float maxMissAngle;
		public float chanceToShoot = 1;

		public override System.Void SendMissiles (string senderTag)
		{
				if (Random.Range (0f, 1f) <= chanceToShoot) {
						float angle = Vector3.Angle (transform.forward, (PlayerShip.instance.transform.position - transform.position).normalized);
						for (int i=0; i<missileIDs.Count; i++) {
								GameObject missile = PullGameObjectManager.PullGameObject (missileIDs [i], transform.position + missilePositions [i], transform.eulerAngles);
								if (missile != null && missile.GetComponent<Missile> ()) {
										missile.GetComponent<Missile> ().damage = Random.Range (randomDamages [i].x, randomDamages [i].y);
										if (transform.InverseTransformPoint (PlayerShip.instance.transform.position).x > 0)
												missile.transform.Rotate (Vector3.up, angle + Random.Range (-maxMissAngle, maxMissAngle));
										else
												missile.transform.Rotate (Vector3.up, -angle + Random.Range (-maxMissAngle, maxMissAngle));
										missile.tag = senderTag;
								}
						}
				}
				//we dont want to send missiles with normal weapon
				//so we remove base.SendMissiles (senderTag);
		}
}
