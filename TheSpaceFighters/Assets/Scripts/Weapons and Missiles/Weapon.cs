﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Weapon : MonoBehaviour
{
		public float cooldown = 0.2f;
	
		//missle informations:
		public List<string> missileIDs;//our missle id to pull
		public List<Vector3> missilePositions;
		public List<float> missileAngles;
		public List<Vector2> randomDamages;
		protected float curCoolTime = 0;

		public virtual void Update ()
		{
				if (curCoolTime > 0)
						curCoolTime -= Time.deltaTime;
		}

		public virtual void Shoot (string senderTag)
		{
				if (curCoolTime <= 0) {
						curCoolTime = cooldown;
						SendMissiles (senderTag);
				}
		}

		public virtual void SendMissiles (string senderTag)
		{
				//fire All At Once
				for (int i=0; i<missileIDs.Count; i++) {
						GameObject missile = PullGameObjectManager.PullGameObject (missileIDs [i], transform.position + missilePositions [i], transform.eulerAngles);
						if (missile != null && missile.GetComponent<Missile> ()) {
								missile.GetComponent<Missile> ().damage = Random.Range (randomDamages [i].x, randomDamages [i].y);
								missile.transform.Rotate (Vector3.up, missileAngles [i]);
								missile.tag = senderTag;
						}
				}

		}
}
