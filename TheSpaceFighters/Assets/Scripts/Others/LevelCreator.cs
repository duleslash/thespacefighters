using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelCreator : MonoBehaviour
{
		public static LevelCreator instance;
		public List<GameObject> bosses;
		public static int index = 0;
		public static List<Creation> spawningList;
		public ShowHideButton gameOverScreen;
		private float curTime = 0;
		private float nextTime = 0;
		
		// Use this for initialization
		void Start ()
		{

				instance = this;

				LevelCreator.spawningList = new List<Creation> ();
				float time = 0;
				
				//enemy creation
				Creation newEnemy;
				for (int i=0; i<30; i++) {
						float rndTime = Random.Range (0.5f, 1.8f);
						float rndSpeed = Random.Range (0.2f, 0.5f);
						float randomX = Random.Range (0.1f, 0.9f);
						newEnemy = new Creation ("Hull", randomX, 1.5f, time + rndTime);
						newEnemy.movements.Add (new Move (randomX, -0.1f, rndSpeed));
						time += rndTime;
				}
				time += 2;
				for (int i=0; i<8; i++) {
						float rndTime = Random.Range (0.3f, 1f);
						float rndSpeed = Random.Range (0.5f, 0.8f);
						float randomY = Random.Range (0.1f, 0.9f);
						newEnemy = new Creation ("Small", -0.2f, randomY, rndTime + time);
						newEnemy.movements.Add (new Move (1.5f, randomY, rndSpeed, true));
						time += i * 0.5f + rndTime;
				}
				time += 1;
				for (int i=0; i<8; i++) {
						newEnemy = new Creation ("Droid", -0.2f, 1.2f, time);
						newEnemy.movements.Add (new Move (1.2f, -0.2f, 0.7f, 0.2f, 4f));
						time += 0.4f;
				}
				time += 6;

				newEnemy = new Creation (bosses [0], 0.5f, 1.2f, time);
				newEnemy.createBoss = null;
				time += 2;

				for (int i=0; i<5; i++) {
						newEnemy = new Creation ("Droid", 1.2f, 1.2f, time);
						newEnemy.movements.Add (new Move (0.2f, -0.2f, 0.7f, 0.2f, 4f));
						time += 0.4f;
				}
				for (int i=0; i<5; i++) {
						newEnemy = new Creation ("Droid", -0.2f, 1.2f, time);
						newEnemy.movements.Add (new Move (1.2f, -0.2f, 0.7f, 0.2f, 4f));
						time += 0.4f;
				}
				time += 2;
				for (int i=0; i<10; i++) {
						float rndTime = Random.Range (0.5f, 1.8f);
						float rndSpeed = Random.Range (0.4f, 0.9f);
						float randomX = Random.Range (0.1f, 0.9f);
						newEnemy = new Creation ("Hull", randomX, 1.5f, time + rndTime);
						newEnemy.movements.Add (new Move (randomX, -0.1f, rndSpeed));
						time += rndTime;
				}

				time += 4;
				for (int i=0; i<10; i++) {
						float rndTime = Random.Range (0.5f, 1.8f);
						float rndSpeed = Random.Range (0.4f, 0.9f);
						float randomX = Random.Range (0.1f, 0.9f);
						newEnemy = new Creation ("Dark", randomX, -0.1f, time + rndTime);
						newEnemy.movements.Add (new Move (randomX, 1.3f, rndSpeed));
						time += rndTime;
				}
				time += 3;
				for (int i=0; i<8; i++) {
						float rndTime = Random.Range (0.3f, 1f);
						float rndSpeed = Random.Range (0.5f, 0.8f);
						float randomY = Random.Range (0.1f, 0.9f);
						newEnemy = new Creation ("Small", 1.2f, randomY, rndTime + time);
						newEnemy.movements.Add (new Move (-0.5f, randomY, rndSpeed, true));
						time += i * 0.5f + rndTime;
				}
				for (int i=0; i<5; i++) {
						newEnemy = new Creation ("Droid", -0.2f, 1.2f, time);
						newEnemy.movements.Add (new Move (1.2f, -0.2f, 0.7f, 0.2f, 4f));
						time += 0.4f;
				}
				time += 2;
				for (int i=0; i<10; i++) {
						float rndTime = Random.Range (0.5f, 1.8f);
						float rndSpeed = Random.Range (0.4f, 0.9f);
						float randomX = Random.Range (0.1f, 0.9f);
						float randomY = Random.Range (0.1f, 0.9f);
						newEnemy = new Creation ("Hull", randomX, 1.5f, time + rndTime);
						newEnemy.movements.Add (new Move (randomX, randomY, rndSpeed));
						newEnemy.movements.Add (new Move (randomX, -0.1f, rndSpeed));
						time += rndTime;
				}
				time += 5;
				newEnemy = new Creation (bosses [1], 0.5f, 1.2f, time);
				newEnemy.createBoss = null;

				time += 2;
				for (int i=0; i<8; i++) {
						float rndTime = Random.Range (0.3f, 1f);
						float rndSpeed = Random.Range (0.5f, 0.8f);
						float randomY = Random.Range (0.1f, 0.9f);
						float randomX = Random.Range (0.1f, 0.9f);
						newEnemy = new Creation ("Small", -0.2f, randomY, rndTime + time);
						newEnemy.movements.Add (new Move (randomX, randomY, rndSpeed, true));
						newEnemy.movements.Add (new Move (1.5f, randomY, rndSpeed, true));
						time += i * 0.5f + rndTime;
				}
				time += 1;
				for (int i=0; i<8; i++) {
						newEnemy = new Creation ("Droid", -0.2f, 1.2f, time);
						newEnemy.movements.Add (new Move (0.5f, 0.2f, 0.7f, 0.2f, 4f));
						newEnemy.movements.Add (new Move (1.2f, 1.2f, 0.7f, 0.2f, 4f));
						time += 0.4f;
				}
				time += 2;
				for (int i=0; i<5; i++) {
						newEnemy = new Creation ("Droid", 1.2f, 1.2f, time);
						newEnemy.movements.Add (new Move (0.5f, 0.2f, 0.7f, 0.2f, 4f));
						newEnemy.movements.Add (new Move (-0.2f, 1.2f, 0.7f, 0.2f, 4f));
						time += 0.4f;
				}
				for (int i=0; i<5; i++) {
						newEnemy = new Creation ("Droid", -0.2f, 1.2f, time);
						newEnemy.movements.Add (new Move (1.2f, -0.2f, 0.7f, 0.2f, 4f));
						time += 0.4f;
				}
				time += 2;
				for (int i=0; i<10; i++) {
						float rndTime = Random.Range (0.5f, 1.8f);
						float rndSpeed = Random.Range (0.4f, 0.9f);
						float randomX = Random.Range (0.1f, 0.9f);
						newEnemy = new Creation ("Hull", randomX, 1.5f, time + rndTime);
						newEnemy.movements.Add (new Move (randomX, -0.1f, rndSpeed));
						time += rndTime;
				}
		
				time += 4;
				for (int i=0; i<10; i++) {
						float rndTime = Random.Range (0.5f, 1.8f);
						float rndSpeed = Random.Range (0.4f, 0.9f);
						float randomX = Random.Range (0.1f, 0.9f);
						newEnemy = new Creation ("Dark", randomX, -0.1f, time + rndTime);
						newEnemy.movements.Add (new Move (randomX, 1.3f, rndSpeed));
						time += rndTime;
				}
				time += 5;
				newEnemy = new Creation (bosses [2], 0.5f, 1.2f, time);
				newEnemy.createBoss = null;

				index = 0;
				nextTime = spawningList [index].time;

		}
	
		// Update is called once per frame
		void Update ()
		{
				curTime += Time.deltaTime;
				if (curTime >= nextTime)
						CreateNext ();
		}
		// this function creates new shop in scene
		public void CreateNext ()
		{
				if (spawningList [index].createBoss == null) {// if its not boss we will pull it
						GameObject newShip = PullGameObjectManager.PullGameObject (spawningList [index].id, spawningList [index].spawnPosition, new Vector3 (90, 180, 0));
						
						Effect lastAddedEffect = null;
						Vector3 lastPosition = spawningList [index].spawnPosition;
						Vector3 position = spawningList [index].movements [0].moveToPosition;// look at first movement point

						newShip.transform.Rotate (Vector3.up, -Vector3.Angle (newShip.transform.forward, (position - newShip.transform.position).normalized));
						// we go in reverse for function to set all movements in right order if there is more then
						for (int i = spawningList[index].movements.Count-1; i>=0; i--) {
								Move m = spawningList [index].movements [i];
								if (m.amplitude == 0 || m.frequency == 0) {
										float duration = Vector3.Distance (lastPosition, m.moveToPosition) / m.speed;
										MoveToEffect me = MoveToEffect.AddComponetWithParametars (newShip, m.moveToPosition, duration, m.delay, lastAddedEffect, m.lookAtMove);
										me.curve = AnimationCurve.Linear (0, 0, 1, 1);					
										lastAddedEffect = me;
								} else {
										float duration = Vector3.Distance (lastPosition, m.moveToPosition) / m.speed;
										WaveMoveToEffect me = WaveMoveToEffect.AddComponetWithParametars (newShip, m.moveToPosition, duration, m.delay, m.amplitude, m.frequency, lastAddedEffect, m.lookAtMove);
										me.curve = AnimationCurve.Linear (0, 0, 1, 1);						
										lastAddedEffect = me;
								}
						}
						if (lastAddedEffect != null)
								lastAddedEffect.PlayEffect ();


				} else {
						// if its boss we should just activate it
						spawningList [index].createBoss.transform.localEulerAngles = new Vector3 (90, 180, 0);
						spawningList [index].createBoss.transform.position = spawningList [index].spawnPosition;
						spawningList [index].createBoss.SetActive (true);

				}
				if (spawningList.Count > index + 1)
						index++;
				else {
						gameOverScreen.OnMouseDown ();
						this.enabled = false;// if its the end of the game
				}
				nextTime = spawningList [index].time;
		}
		//structure of pull manager
		public struct Creation
		{
				public GameObject createBoss;
				public string id;
				public Vector2 spawnPosition;
				public float time;
				public List<Move> movements;
		
				// constructor
				public Creation (string pullID, float viewPortX, float viewPortY, float spawningTime, List<Move> movementEffects)
				{
						id = pullID;
						spawnPosition = GetPosition (viewPortX, viewPortY);
						time = spawningTime;
						movements = new List<Move> ();
						movements.AddRange (movementEffects);
						LevelCreator.spawningList.Add (this);
						LevelCreator.spawningList.Sort (delegate(Creation c1, Creation c2) {
								return c1.time.CompareTo (c2.time);
						});
						createBoss = null;
				}
				// constructor
				public Creation (string pullID, float viewPortX, float viewPortY, float spawningTime, Move movementEffect)
				{
						id = pullID;
						spawnPosition = GetPosition (viewPortX, viewPortY);
						time = spawningTime;
						movements = new List<Move> ();
						movements.Add (movementEffect);
						LevelCreator.spawningList.Add (this);
						LevelCreator.spawningList.Sort (delegate(Creation c1, Creation c2) {
								return c1.time.CompareTo (c2.time);
						});
						createBoss = null;
				}
				// constructor
				public Creation (string pullID, float viewPortX, float viewPortY, float spawningTime)
				{
						id = pullID;
						spawnPosition = GetPosition (viewPortX, viewPortY);
						time = spawningTime;
						movements = new List<Move> ();
						LevelCreator.spawningList.Add (this);
						LevelCreator.spawningList.Sort (delegate(Creation c1, Creation c2) {
								return c1.time.CompareTo (c2.time);
						});
						createBoss = null;
				}
				// bosses constructor
				public Creation (GameObject boss, float viewPortX, float viewPortY, float spawningTime)
				{
						id = null;
						movements = new List<Move> ();
						createBoss = boss;
						spawnPosition = GetPosition (viewPortX, viewPortY);
						time = spawningTime;
						LevelCreator.spawningList.Add (this);
						LevelCreator.spawningList.Sort (delegate(Creation c1, Creation c2) {
								return c1.time.CompareTo (c2.time);
						});
				}
		}
		//structure of movement
		public struct Move
		{
				public Vector2 moveToPosition;
				public float speed;
				public float delay;
				public float amplitude;
				public float frequency;
				public bool lookAtMove;
				// constructor
				public Move (float viewPortX, float viewPortY, float moveSpeed, float effectDelay, float effectAmplitude, float effectFrequency, bool lookAt)
				{
						moveToPosition = GetPosition (viewPortX, viewPortY);
						delay = effectDelay;
						speed = moveSpeed;
						amplitude = effectAmplitude;
						frequency = effectFrequency;
						lookAtMove = lookAt;
				}
				// constructor
				public Move (float viewPortX, float viewPortY, float moveSpeed, float effectAmplitude, float effectFrequency, bool lookAt)
				{
						moveToPosition = GetPosition (viewPortX, viewPortY);
						delay = 0;
						speed = moveSpeed;
						amplitude = effectAmplitude;
						frequency = effectFrequency;
						lookAtMove = lookAt;
				}
				// constructor
				public Move (float viewPortX, float viewPortY, float moveSpeed, float effectAmplitude, float effectFrequency)
				{
						moveToPosition = GetPosition (viewPortX, viewPortY);
						delay = 0;
						speed = moveSpeed;
						amplitude = effectAmplitude;
						frequency = effectFrequency;
						lookAtMove = false;
				}
				// constructor
				public Move (float viewPortX, float viewPortY, float moveSpeed, bool lookAt)
				{
						moveToPosition = GetPosition (viewPortX, viewPortY);
						delay = 0;
						speed = moveSpeed;
						amplitude = 0;
						frequency = 0;
						lookAtMove = lookAt;
				}
				// constructor
				public Move (float viewPortX, float viewPortY, float moveSpeed)
				{
						moveToPosition = GetPosition (viewPortX, viewPortY);
						delay = 0;
						speed = moveSpeed;
						amplitude = 0;
						frequency = 0;
						lookAtMove = false;
				}
		}
		//get position based on viewport point
		public static Vector2 GetPosition (float viewPortPointX, float viewPortPointY)
		{	
				Vector3 position = Camera.main.ViewportToWorldPoint (new Vector3 (viewPortPointX, viewPortPointY, 0));
				return new Vector2 (position.x, position.y);
		}

		public static Vector2 GetPosition (Vector2 viewportposition)
		{	
				Vector3 position = Camera.main.ViewportToWorldPoint (new Vector3 (viewportposition.x, viewportposition.y, 0));
				return new Vector2 (position.x, position.y);
		}
		//get screen bounds
		public static Vector4 GetScreenBounds (float bound)
		{
				Vector2 topRight = GetPosition (bound, bound);
				Vector2 bottomLeft = GetPosition (1 - bound, 1 - bound);
				//Debug.Log (new Vector4 (bottomLeft.x, bottomLeft.y, topRight.x, topRight.y));
				return new Vector4 (bottomLeft.x, bottomLeft.y, topRight.x, topRight.y);
		}
}
