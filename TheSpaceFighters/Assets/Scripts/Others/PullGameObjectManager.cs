﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class PullGameObjectManager : MonoBehaviour
{

		// unique id of this pull manager
		public string id;
		private List<GameObject> readyGameObjects;

		void Awake ()
		{
				managers = new List<PullManager> ();
		}

		void Start ()
		{

				// Add this pull manager to static list of all managers at start
				PullManager thisManager = new PullManager (id, this);
				managers.Add (thisManager);

				// Add all gameobjects to list we use to pull object
				readyGameObjects = new List<GameObject> ();
				for (int i=0; i<transform.childCount; i++)
						readyGameObjects.Add (transform.GetChild (i).gameObject);
		}

		// pulling object from this manager and return it
		public GameObject PullNextGameObject (Vector3 position, Vector3 eulerRotation)
		{
				if (readyGameObjects.Count < 2) {//we are  almost ran out of objects we could pull, so we better create more	
						GameObject newGO = Instantiate (readyGameObjects [0], readyGameObjects [0].transform.position, readyGameObjects [0].transform.rotation)as GameObject;
						newGO.transform.parent = transform;
						readyGameObjects.Add (newGO);
				}
				//we pull first free object
				GameObject pulledObject = readyGameObjects [0];
				//set position and location
				pulledObject.transform.position = position;
				pulledObject.transform.eulerAngles = eulerRotation;
				//turn on
				pulledObject.SetActive (true);
				//remove from list since its already pulled
				readyGameObjects.RemoveAt (0);
				return pulledObject;
		}


		//static part
		//to pull object easy

		//structure of pull manager
		public struct PullManager
		{
				public string id;
				public PullGameObjectManager instance;

				// constructor
				public PullManager (string pullID, PullGameObjectManager pullInstance)
				{
						id = pullID;
						instance = pullInstance;
				}
		}

		//static list with all pull managers we could use to pull object from
		public static List<PullManager> managers;
	
		// static function that locates our manager and returns the object pulled from it
		public static GameObject PullGameObject (string id, Vector2 position, Vector3 eulerRotation)
		{
				foreach (PullManager manager in managers) {
						if (manager.id == id) { // we found responsible manager for this id and we pulled object from it
								return manager.instance.PullNextGameObject (position, eulerRotation);
						}
				}
				// we dont have manager with that id so we return null
				return null;

		}

		public void ReturnGOToPull (GameObject gObject)
		{
				gObject.SetActive (false);
				readyGameObjects.Add (gObject);
		}
}
