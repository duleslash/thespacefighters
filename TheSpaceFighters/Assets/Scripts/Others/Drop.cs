﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Drop : MonoBehaviour
{



		public List<GameObject> graphics;
		private int graphicsZ = 45;
		public float trackingRange = 0.5F;
		public float trackingSpeed = 5;
		private Ship trackingTarget;
		private int choice;
		private float duration = 5;

		void OnEnable ()
		{
				duration = 5;
				float chance = Random.Range (0f, 1f);

				choice = 0;//score
				if (chance < 0.02f)
						choice = 3;//life
				else if (chance < 0.04f)
						choice = 2;//heal
				else if (chance < 0.2f)
						choice = 1;//weapon
			

				for (int i=0; i<graphics.Count; i++) {
						if (choice == i)
								graphics [i].transform.localPosition += Vector3.forward * graphicsZ;
						else
								graphics [i].SetActive (false);
				}
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (duration <= 0)
						Reset ();
				duration -= Time.deltaTime;
				if (Vector3.Distance (PlayerShip.instance.transform.position, transform.position) < trackingRange) {
						transform.position += (PlayerShip.instance.transform.position - transform.position).normalized * Time.deltaTime * trackingSpeed;
				}
				if (Vector3.Distance (PlayerShip.instance.transform.position, transform.position) < 0.1f)
						GiveUpgrade ();
		}

		void GiveUpgrade ()
		{
				switch (choice) {
				case 0:
						PlayerShip.instance.score += Random.Range (40, 80);
						break;
				case 1:
						PlayerShip.instance.UpgradeWeapon ();
						break;
				case 2:
						PlayerShip.instance.hp = PlayerShip.instance.maxHp;
						break;
				case 3:
						PlayerShip.instance.lives++;
						break;
				}

				Reset ();
		}

		void Reset ()
		{
				for (int i=0; i<graphics.Count; i++) {
						if (choice == i)
								graphics [i].transform.localPosition = Vector3.zero;
						else
								graphics [i].SetActive (true);
				}
				transform.parent.GetComponent<PullGameObjectManager> ().ReturnGOToPull (gameObject);
		}
}
