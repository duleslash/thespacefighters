﻿using UnityEngine;
using System.Collections;

public class ResetObject : MonoBehaviour
{

		public float delay = 0.5f;
		private float realDelay;

		void OnEnable ()
		{
				realDelay = delay;
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (realDelay <= 0)
						Reset ();
				realDelay -= Time.deltaTime;
		}

		void Reset ()
		{
				transform.parent.GetComponent<PullGameObjectManager> ().ReturnGOToPull (gameObject);
		}
}
