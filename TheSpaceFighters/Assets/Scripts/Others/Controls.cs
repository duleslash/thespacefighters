﻿using UnityEngine;
using System.Collections;

public class Controls : MonoBehaviour
{

		public static bool isMouse;
		private Vector3 mouseMovingPosition;
		private Vector3 lastMousuePosition;
		// Use this for initialization
		void Start ()
		{
	
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (isMouse) {
						mouseMovingPosition = Vector3.Lerp (mouseMovingPosition, Vector3.zero, Time.deltaTime * 20);
						Vector3 mousePosition = Camera.main.ScreenToViewportPoint (Input.mousePosition);
						if (Input.GetMouseButton (1)) {
								mouseMovingPosition += (mousePosition - lastMousuePosition) * Time.deltaTime * 3000;
								if (mouseMovingPosition.magnitude > 1)
										mouseMovingPosition.Normalize ();
						}
						
						lastMousuePosition = mousePosition;
				}
		}

		public Vector2 GetAxis ()
		{
				Vector2 value = Vector2.zero;

				if (isMouse)
						value = new Vector2( mouseMovingPosition.x,mouseMovingPosition.y);
				 else 
						value = new Vector2(Input.GetAxis ("Horizontal"),Input.GetAxis ("Vertical"));
				
				return value;
		}

		public bool GetFireKey ()
		{
				if (isMouse)
						return Input.GetMouseButton (0);
				else
						return Input.GetKey (KeyCode.Space);
		}
}
